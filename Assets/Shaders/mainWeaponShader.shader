// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:3,spmd:0,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:True,hqlp:False,rprd:True,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:2865,x:33675,y:32755,varname:node_2865,prsc:2|diff-7139-RGB,spec-5127-OUT,gloss-5269-A,normal-5964-RGB,emission-9896-OUT;n:type:ShaderForge.SFN_Tex2d,id:5964,x:33129,y:32775,ptovrint:True,ptlb:Normal Map,ptin:_BumpMap,varname:_BumpMap,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:23d5a9a9dcc918a49bbf93fd4e39d77d,ntxv:3,isnm:True;n:type:ShaderForge.SFN_VertexColor,id:269,x:30967,y:32372,varname:node_269,prsc:2;n:type:ShaderForge.SFN_Vector1,id:7108,x:31588,y:33167,varname:node_7108,prsc:2,v1:0.1;n:type:ShaderForge.SFN_Vector1,id:7730,x:31858,y:33002,varname:node_7730,prsc:2,v1:0.5;n:type:ShaderForge.SFN_Vector1,id:4740,x:31216,y:32809,varname:node_4740,prsc:2,v1:0.5;n:type:ShaderForge.SFN_Subtract,id:3633,x:32061,y:32958,varname:node_3633,prsc:2|A-269-A,B-7730-OUT;n:type:ShaderForge.SFN_Sign,id:6209,x:32229,y:32958,varname:node_6209,prsc:2|IN-3633-OUT;n:type:ShaderForge.SFN_Clamp01,id:164,x:32398,y:32958,varname:node_164,prsc:2|IN-6209-OUT;n:type:ShaderForge.SFN_Lerp,id:3859,x:32881,y:32847,varname:node_3859,prsc:2|A-464-OUT,B-5876-OUT,T-164-OUT;n:type:ShaderForge.SFN_Vector3,id:9498,x:31844,y:32701,varname:node_9498,prsc:2,v1:1,v2:1,v3:1;n:type:ShaderForge.SFN_Fmod,id:34,x:31415,y:32795,cmnt:0 to .5,varname:node_34,prsc:2|A-269-A,B-4740-OUT;n:type:ShaderForge.SFN_Multiply,id:5876,x:32563,y:32897,varname:node_5876,prsc:2|A-9498-OUT,B-3977-OUT;n:type:ShaderForge.SFN_Multiply,id:464,x:32563,y:32747,varname:node_464,prsc:2|A-7040-OUT,B-3977-OUT;n:type:ShaderForge.SFN_Multiply,id:3977,x:31659,y:32795,varname:node_3977,prsc:2|A-34-OUT,B-266-OUT;n:type:ShaderForge.SFN_Vector1,id:266,x:31266,y:32666,varname:node_266,prsc:2,v1:2;n:type:ShaderForge.SFN_Fmod,id:7395,x:31811,y:33124,varname:node_7395,prsc:2|A-3977-OUT,B-7108-OUT;n:type:ShaderForge.SFN_Multiply,id:5540,x:32030,y:33124,varname:node_5540,prsc:2|A-7395-OUT,B-9456-OUT;n:type:ShaderForge.SFN_Vector1,id:9456,x:31763,y:33297,varname:node_9456,prsc:2,v1:10;n:type:ShaderForge.SFN_Fmod,id:8512,x:32232,y:32118,varname:node_8512,prsc:2|A-269-R,B-4566-OUT;n:type:ShaderForge.SFN_Fmod,id:4109,x:32232,y:32325,varname:node_4109,prsc:2|A-269-G,B-4566-OUT;n:type:ShaderForge.SFN_Fmod,id:5671,x:32232,y:32452,varname:node_5671,prsc:2|A-269-B,B-4566-OUT;n:type:ShaderForge.SFN_Vector1,id:4566,x:31681,y:32338,varname:node_4566,prsc:2,v1:0.02;n:type:ShaderForge.SFN_Multiply,id:9454,x:32460,y:32160,varname:node_9454,prsc:2|A-8512-OUT,B-9648-OUT;n:type:ShaderForge.SFN_Multiply,id:2541,x:32440,y:32313,varname:node_2541,prsc:2|A-4109-OUT,B-9648-OUT;n:type:ShaderForge.SFN_Multiply,id:202,x:32440,y:32452,varname:node_202,prsc:2|A-5671-OUT,B-9648-OUT;n:type:ShaderForge.SFN_Vector1,id:9648,x:31704,y:32213,varname:node_9648,prsc:2,v1:50;n:type:ShaderForge.SFN_Append,id:7040,x:32624,y:32587,varname:node_7040,prsc:2|A-6297-OUT,B-4109-OUT,C-5671-OUT;n:type:ShaderForge.SFN_Multiply,id:650,x:32128,y:31700,varname:node_650,prsc:2|A-269-R,B-9648-OUT;n:type:ShaderForge.SFN_Trunc,id:3883,x:32289,y:31700,varname:node_3883,prsc:2|IN-650-OUT;n:type:ShaderForge.SFN_Multiply,id:8317,x:32460,y:31700,varname:node_8317,prsc:2|A-3883-OUT,B-4566-OUT;n:type:ShaderForge.SFN_Append,id:6972,x:32342,y:31562,varname:node_6972,prsc:2|A-8317-OUT,B-3985-OUT,C-2628-OUT;n:type:ShaderForge.SFN_Multiply,id:468,x:32128,y:31845,varname:node_468,prsc:2|A-269-G,B-9648-OUT;n:type:ShaderForge.SFN_Trunc,id:663,x:32289,y:31845,varname:node_663,prsc:2|IN-468-OUT;n:type:ShaderForge.SFN_Multiply,id:3985,x:32460,y:31845,varname:node_3985,prsc:2|A-663-OUT,B-4566-OUT;n:type:ShaderForge.SFN_Multiply,id:1815,x:32128,y:31990,varname:node_1815,prsc:2|A-269-B,B-9648-OUT;n:type:ShaderForge.SFN_Trunc,id:877,x:32289,y:31990,varname:node_877,prsc:2|IN-1815-OUT;n:type:ShaderForge.SFN_Multiply,id:2628,x:32460,y:31990,varname:node_2628,prsc:2|A-877-OUT,B-4566-OUT;n:type:ShaderForge.SFN_Clamp01,id:2626,x:32624,y:32284,varname:node_2626,prsc:2|IN-2541-OUT;n:type:ShaderForge.SFN_Clamp01,id:1435,x:32624,y:32452,varname:node_1435,prsc:2|IN-202-OUT;n:type:ShaderForge.SFN_Multiply,id:4245,x:31681,y:32425,varname:node_4245,prsc:2|A-269-R,B-7833-OUT;n:type:ShaderForge.SFN_Vector1,id:7833,x:31504,y:32499,varname:node_7833,prsc:2,v1:50;n:type:ShaderForge.SFN_Vector1,id:1536,x:31748,y:32631,varname:node_1536,prsc:2,v1:2;n:type:ShaderForge.SFN_Divide,id:6297,x:32167,y:32593,varname:node_6297,prsc:2|A-4245-OUT,B-7833-OUT;n:type:ShaderForge.SFN_Subtract,id:589,x:32039,y:32452,varname:node_589,prsc:2|A-4245-OUT,B-5791-OUT;n:type:ShaderForge.SFN_Trunc,id:5791,x:31856,y:32375,varname:node_5791,prsc:2|IN-4245-OUT;n:type:ShaderForge.SFN_Append,id:5127,x:34393,y:32244,cmnt:SPECULAR,varname:node_5127,prsc:2|A-327-OUT,B-9837-OUT,C-9512-OUT;n:type:ShaderForge.SFN_Multiply,id:9020,x:33110,y:31721,varname:node_9020,prsc:2|A-7139-R,B-7105-OUT;n:type:ShaderForge.SFN_Vector1,id:7105,x:32773,y:31738,varname:node_7105,prsc:2,v1:1000;n:type:ShaderForge.SFN_Subtract,id:327,x:33507,y:31753,varname:node_327,prsc:2|A-9020-OUT,B-7988-OUT;n:type:ShaderForge.SFN_Multiply,id:7615,x:33110,y:31921,varname:node_7615,prsc:2|A-7139-G,B-7105-OUT;n:type:ShaderForge.SFN_Subtract,id:9837,x:33507,y:31922,varname:node_9837,prsc:2|A-7615-OUT,B-1804-OUT;n:type:ShaderForge.SFN_Trunc,id:7988,x:33269,y:31786,varname:node_7988,prsc:2|IN-9020-OUT;n:type:ShaderForge.SFN_Trunc,id:1804,x:33269,y:31988,varname:node_1804,prsc:2|IN-7615-OUT;n:type:ShaderForge.SFN_Multiply,id:6563,x:33110,y:32260,varname:node_6563,prsc:2|A-7139-B,B-7105-OUT;n:type:ShaderForge.SFN_Trunc,id:7368,x:33303,y:32331,varname:node_7368,prsc:2|IN-6563-OUT;n:type:ShaderForge.SFN_Subtract,id:9512,x:33481,y:32256,varname:node_9512,prsc:2|A-6563-OUT,B-7368-OUT;n:type:ShaderForge.SFN_VertexColor,id:7139,x:32588,y:31704,varname:node_7139,prsc:2;n:type:ShaderForge.SFN_Multiply,id:8680,x:32353,y:33366,varname:node_8680,prsc:2|A-5269-A,B-4684-OUT;n:type:ShaderForge.SFN_Vector1,id:4684,x:32164,y:33400,varname:node_4684,prsc:2,v1:100;n:type:ShaderForge.SFN_Trunc,id:3213,x:32532,y:33392,varname:node_3213,prsc:2|IN-8680-OUT;n:type:ShaderForge.SFN_Subtract,id:8207,x:32738,y:33355,varname:node_8207,prsc:2|A-8680-OUT,B-3213-OUT;n:type:ShaderForge.SFN_Lerp,id:9896,x:33277,y:33250,varname:node_9896,prsc:2|A-6480-OUT,B-649-OUT,T-4808-OUT;n:type:ShaderForge.SFN_Multiply,id:8182,x:32377,y:33550,varname:node_8182,prsc:2|A-5269-A,B-2145-OUT;n:type:ShaderForge.SFN_Vector1,id:2145,x:32204,y:33683,varname:node_2145,prsc:2,v1:10000;n:type:ShaderForge.SFN_Trunc,id:3702,x:32532,y:33611,varname:node_3702,prsc:2|IN-8182-OUT;n:type:ShaderForge.SFN_Subtract,id:4953,x:32764,y:33557,varname:node_4953,prsc:2|A-8182-OUT,B-3702-OUT;n:type:ShaderForge.SFN_Vector3,id:6159,x:32628,y:33197,varname:node_6159,prsc:2,v1:1,v2:1,v3:1;n:type:ShaderForge.SFN_Multiply,id:649,x:32912,y:33184,varname:node_649,prsc:2|A-6159-OUT,B-3145-OUT;n:type:ShaderForge.SFN_Multiply,id:6480,x:33153,y:33061,varname:node_6480,prsc:2|A-7139-RGB,B-3145-OUT;n:type:ShaderForge.SFN_VertexColor,id:5269,x:31850,y:33742,varname:node_5269,prsc:2;n:type:ShaderForge.SFN_Clamp,id:7871,x:33787,y:31786,varname:node_7871,prsc:2|IN-327-OUT,MIN-5771-OUT,MAX-5463-OUT;n:type:ShaderForge.SFN_Clamp,id:9104,x:33787,y:31988,varname:node_9104,prsc:2|IN-9837-OUT,MIN-5771-OUT,MAX-5463-OUT;n:type:ShaderForge.SFN_Clamp,id:7732,x:33787,y:32172,varname:node_7732,prsc:2|IN-9512-OUT,MIN-5771-OUT,MAX-5463-OUT;n:type:ShaderForge.SFN_Vector1,id:5771,x:33430,y:32085,varname:node_5771,prsc:2,v1:0.001;n:type:ShaderForge.SFN_Vector1,id:5463,x:33455,y:32167,varname:node_5463,prsc:2,v1:0.999;n:type:ShaderForge.SFN_Clamp,id:4283,x:33234,y:32522,varname:node_4283,prsc:2|IN-7139-RGB,MIN-5771-OUT,MAX-5463-OUT;n:type:ShaderForge.SFN_ConstantClamp,id:4808,x:33001,y:33532,varname:node_4808,prsc:2,min:0.01,max:50|IN-4953-OUT;n:type:ShaderForge.SFN_ConstantClamp,id:3145,x:32912,y:33355,varname:node_3145,prsc:2,min:0.001,max:1|IN-8207-OUT;n:type:ShaderForge.SFN_Vector1,id:1076,x:31793,y:33572,varname:node_1076,prsc:2,v1:0.500101;n:type:ShaderForge.SFN_Add,id:9305,x:33394,y:33611,varname:node_9305,prsc:2|A-4808-OUT,B-1494-OUT;n:type:ShaderForge.SFN_Vector1,id:1494,x:33009,y:33840,varname:node_1494,prsc:2,v1:0;n:type:ShaderForge.SFN_Add,id:6330,x:33562,y:33531,varname:node_6330,prsc:2|A-3145-OUT,B-1494-OUT;n:type:ShaderForge.SFN_Multiply,id:2124,x:32764,y:33796,varname:node_2124,prsc:2|A-3702-OUT,B-7420-OUT;n:type:ShaderForge.SFN_Vector1,id:7420,x:32358,y:33784,varname:node_7420,prsc:2,v1:0;proporder:5964;pass:END;sub:END;*/

Shader "Shader Forge/mainWeaponShader" {
    Properties {
        _BumpMap ("Normal Map", 2D) = "bump" {}
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform sampler2D _BumpMap; uniform float4 _BumpMap_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float2 uv2 : TEXCOORD2;
                float4 posWorld : TEXCOORD3;
                float3 normalDir : TEXCOORD4;
                float3 tangentDir : TEXCOORD5;
                float3 bitangentDir : TEXCOORD6;
                float4 vertexColor : COLOR;
                LIGHTING_COORDS(7,8)
                UNITY_FOG_COORDS(9)
                #if defined(LIGHTMAP_ON) || defined(UNITY_SHOULD_SAMPLE_SH)
                    float4 ambientOrLightmapUV : TEXCOORD10;
                #endif
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                o.vertexColor = v.vertexColor;
                #ifdef LIGHTMAP_ON
                    o.ambientOrLightmapUV.xy = v.texcoord1.xy * unity_LightmapST.xy + unity_LightmapST.zw;
                    o.ambientOrLightmapUV.zw = 0;
                #elif UNITY_SHOULD_SAMPLE_SH
                #endif
                #ifdef DYNAMICLIGHTMAP_ON
                    o.ambientOrLightmapUV.zw = v.texcoord2.xy * unity_DynamicLightmapST.xy + unity_DynamicLightmapST.zw;
                #endif
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _BumpMap_var = UnpackNormal(tex2D(_BumpMap,TRANSFORM_TEX(i.uv0, _BumpMap)));
                float3 normalLocal = _BumpMap_var.rgb;
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
                float Pi = 3.141592654;
                float InvPi = 0.31830988618;
///////// Gloss:
                float gloss = i.vertexColor.a;
                float specPow = exp2( gloss * 10.0+1.0);
/////// GI Data:
                UnityLight light;
                #ifdef LIGHTMAP_OFF
                    light.color = lightColor;
                    light.dir = lightDirection;
                    light.ndotl = LambertTerm (normalDirection, light.dir);
                #else
                    light.color = half3(0.f, 0.f, 0.f);
                    light.ndotl = 0.0f;
                    light.dir = half3(0.f, 0.f, 0.f);
                #endif
                UnityGIInput d;
                d.light = light;
                d.worldPos = i.posWorld.xyz;
                d.worldViewDir = viewDirection;
                d.atten = attenuation;
                #if defined(LIGHTMAP_ON) || defined(DYNAMICLIGHTMAP_ON)
                    d.ambient = 0;
                    d.lightmapUV = i.ambientOrLightmapUV;
                #else
                    d.ambient = i.ambientOrLightmapUV;
                #endif
                d.boxMax[0] = unity_SpecCube0_BoxMax;
                d.boxMin[0] = unity_SpecCube0_BoxMin;
                d.probePosition[0] = unity_SpecCube0_ProbePosition;
                d.probeHDR[0] = unity_SpecCube0_HDR;
                d.boxMax[1] = unity_SpecCube1_BoxMax;
                d.boxMin[1] = unity_SpecCube1_BoxMin;
                d.probePosition[1] = unity_SpecCube1_ProbePosition;
                d.probeHDR[1] = unity_SpecCube1_HDR;
                Unity_GlossyEnvironmentData ugls_en_data;
                ugls_en_data.roughness = 1.0 - gloss;
                ugls_en_data.reflUVW = viewReflectDirection;
                UnityGI gi = UnityGlobalIllumination(d, 1, normalDirection, ugls_en_data );
                lightDirection = gi.light.dir;
                lightColor = gi.light.color;
////// Specular:
                float NdotL = max(0, dot( normalDirection, lightDirection ));
                float LdotH = max(0.0,dot(lightDirection, halfDirection));
                float node_7105 = 1000.0;
                float node_9020 = (i.vertexColor.r*node_7105);
                float node_327 = (node_9020-trunc(node_9020));
                float node_7615 = (i.vertexColor.g*node_7105);
                float node_9837 = (node_7615-trunc(node_7615));
                float node_6563 = (i.vertexColor.b*node_7105);
                float node_9512 = (node_6563-trunc(node_6563));
                float3 specularColor = float3(node_327,node_9837,node_9512);
                float specularMonochrome = max( max(specularColor.r, specularColor.g), specularColor.b);
                float NdotV = max(0.0,dot( normalDirection, viewDirection ));
                float NdotH = max(0.0,dot( normalDirection, halfDirection ));
                float VdotH = max(0.0,dot( viewDirection, halfDirection ));
                float visTerm = SmithBeckmannVisibilityTerm( NdotL, NdotV, 1.0-gloss );
                float normTerm = max(0.0, NDFBlinnPhongNormalizedTerm(NdotH, RoughnessToSpecPower(1.0-gloss)));
                float specularPBL = max(0, (NdotL*visTerm*normTerm) * (UNITY_PI / 4) );
                float3 directSpecular = 1 * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularPBL*lightColor*FresnelTerm(specularColor, LdotH);
                half grazingTerm = saturate( gloss + specularMonochrome );
                float3 indirectSpecular = (gi.indirect.specular);
                indirectSpecular *= FresnelLerp (specularColor, grazingTerm, NdotV);
                float3 specular = (directSpecular + indirectSpecular);
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                half fd90 = 0.5 + 2 * LdotH * LdotH * (1-gloss);
                float3 directDiffuse = ((1 +(fd90 - 1)*pow((1.00001-NdotL), 5)) * (1 + (fd90 - 1)*pow((1.00001-NdotV), 5)) * NdotL) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += gi.indirect.diffuse;
                float3 diffuseColor = i.vertexColor.rgb;
                diffuseColor *= 1-specularMonochrome;
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
////// Emissive:
                float node_8680 = (i.vertexColor.a*100.0);
                float node_3145 = clamp((node_8680-trunc(node_8680)),0.001,1);
                float node_8182 = (i.vertexColor.a*10000.0);
                float node_3702 = trunc(node_8182);
                float node_4808 = clamp((node_8182-node_3702),0.01,50);
                float3 emissive = lerp((i.vertexColor.rgb*node_3145),(float3(1,1,1)*node_3145),node_4808);
/// Final Color:
                float3 finalColor = diffuse + specular + emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform sampler2D _BumpMap; uniform float4 _BumpMap_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float2 uv2 : TEXCOORD2;
                float4 posWorld : TEXCOORD3;
                float3 normalDir : TEXCOORD4;
                float3 tangentDir : TEXCOORD5;
                float3 bitangentDir : TEXCOORD6;
                float4 vertexColor : COLOR;
                LIGHTING_COORDS(7,8)
                UNITY_FOG_COORDS(9)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                o.vertexColor = v.vertexColor;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 _BumpMap_var = UnpackNormal(tex2D(_BumpMap,TRANSFORM_TEX(i.uv0, _BumpMap)));
                float3 normalLocal = _BumpMap_var.rgb;
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
                float Pi = 3.141592654;
                float InvPi = 0.31830988618;
///////// Gloss:
                float gloss = i.vertexColor.a;
                float specPow = exp2( gloss * 10.0+1.0);
////// Specular:
                float NdotL = max(0, dot( normalDirection, lightDirection ));
                float LdotH = max(0.0,dot(lightDirection, halfDirection));
                float node_7105 = 1000.0;
                float node_9020 = (i.vertexColor.r*node_7105);
                float node_327 = (node_9020-trunc(node_9020));
                float node_7615 = (i.vertexColor.g*node_7105);
                float node_9837 = (node_7615-trunc(node_7615));
                float node_6563 = (i.vertexColor.b*node_7105);
                float node_9512 = (node_6563-trunc(node_6563));
                float3 specularColor = float3(node_327,node_9837,node_9512);
                float specularMonochrome = max( max(specularColor.r, specularColor.g), specularColor.b);
                float NdotV = max(0.0,dot( normalDirection, viewDirection ));
                float NdotH = max(0.0,dot( normalDirection, halfDirection ));
                float VdotH = max(0.0,dot( viewDirection, halfDirection ));
                float visTerm = SmithBeckmannVisibilityTerm( NdotL, NdotV, 1.0-gloss );
                float normTerm = max(0.0, NDFBlinnPhongNormalizedTerm(NdotH, RoughnessToSpecPower(1.0-gloss)));
                float specularPBL = max(0, (NdotL*visTerm*normTerm) * (UNITY_PI / 4) );
                float3 directSpecular = attenColor * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularPBL*lightColor*FresnelTerm(specularColor, LdotH);
                float3 specular = directSpecular;
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                half fd90 = 0.5 + 2 * LdotH * LdotH * (1-gloss);
                float3 directDiffuse = ((1 +(fd90 - 1)*pow((1.00001-NdotL), 5)) * (1 + (fd90 - 1)*pow((1.00001-NdotV), 5)) * NdotL) * attenColor;
                float3 diffuseColor = i.vertexColor.rgb;
                diffuseColor *= 1-specularMonochrome;
                float3 diffuse = directDiffuse * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse + specular;
                fixed4 finalRGBA = fixed4(finalColor * 1,0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "Meta"
            Tags {
                "LightMode"="Meta"
            }
            Cull Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_META 1
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #include "UnityMetaPass.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv1 : TEXCOORD0;
                float2 uv2 : TEXCOORD1;
                float4 posWorld : TEXCOORD2;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                o.vertexColor = v.vertexColor;
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityMetaVertexPosition(v.vertex, v.texcoord1.xy, v.texcoord2.xy, unity_LightmapST, unity_DynamicLightmapST );
                return o;
            }
            float4 frag(VertexOutput i) : SV_Target {
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                UnityMetaInput o;
                UNITY_INITIALIZE_OUTPUT( UnityMetaInput, o );
                
                float node_8680 = (i.vertexColor.a*100.0);
                float node_3145 = clamp((node_8680-trunc(node_8680)),0.001,1);
                float node_8182 = (i.vertexColor.a*10000.0);
                float node_3702 = trunc(node_8182);
                float node_4808 = clamp((node_8182-node_3702),0.01,50);
                o.Emission = lerp((i.vertexColor.rgb*node_3145),(float3(1,1,1)*node_3145),node_4808);
                
                float3 diffColor = i.vertexColor.rgb;
                float node_7105 = 1000.0;
                float node_9020 = (i.vertexColor.r*node_7105);
                float node_327 = (node_9020-trunc(node_9020));
                float node_7615 = (i.vertexColor.g*node_7105);
                float node_9837 = (node_7615-trunc(node_7615));
                float node_6563 = (i.vertexColor.b*node_7105);
                float node_9512 = (node_6563-trunc(node_6563));
                float3 specColor = float3(node_327,node_9837,node_9512);
                float specularMonochrome = max(max(specColor.r, specColor.g),specColor.b);
                diffColor *= (1.0-specularMonochrome);
                float roughness = 1.0 - i.vertexColor.a;
                o.Albedo = diffColor + specColor * roughness * roughness * 0.5;
                
                return UnityMetaFragment( o );
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
