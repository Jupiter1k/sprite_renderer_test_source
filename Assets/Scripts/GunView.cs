﻿using UnityEngine;
using System.Collections;

public class GunView : MonoBehaviour 
{
    //public GameObject projectilePrefab;
    //private Transform myTransform;
    public GameObject barrelEnd;
    //private AudioSource gunAudioSource;
    //private bool inHands = false;

    void Start()
    {
        SetInitialReferences();
    }

    void SetInitialReferences()
    {
        //myTransform = transform;
        //gunAudioSource = GetComponent<AudioSource>();
    }

    //void Update()
    //{
    //    if (inHands && Input.GetButtonDown("Fire1"))
    //    {
    //        FireWeapon();
    //    }
    //}

    public void FireWeapon(GameObject projectilePrefab, AudioClip launchSound, AudioSource audioSource)
    {
        GameObject projectile = (GameObject)Instantiate(projectilePrefab, barrelEnd.transform.TransformPoint(0, 0, .3f), barrelEnd.transform.rotation);

        if (projectile.GetComponent<Projectile>().PropulsionType == PropulsionMode.Propelled)
        {
            projectile.GetComponent<Rigidbody>().velocity = barrelEnd.transform.forward * projectile.GetComponent<Projectile>().PropulsionSpeed;
        }
        else
        {
            projectile.GetComponent<Rigidbody>().velocity = barrelEnd.transform.forward * projectile.GetComponent<Projectile>().LaunchPower;
        }
        audioSource.PlayOneShot(launchSound);
    }
}
