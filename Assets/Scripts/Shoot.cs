﻿using UnityEngine;
using System.Collections;

public class Shoot : MonoBehaviour 
{
    private float fireRate = 0.75f;
    private float nextFire = 0.0f;
    private RaycastHit hit;
    private float range = 300f;
    private Transform myTransform;

	// Use this for initialization
	void Start () 
    {
        SetInitialReferences();
	}
	
	// Update is called once per frame
	void Update () 
    {
        CheckforInput();
	}

    void SetInitialReferences()
    {
        myTransform = transform;
    }

    void CheckforInput()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0) && Time.time > nextFire)
        {
            //Debug.DrawRay(myTransform.TransformPoint(0, 0, 1), myTransform.forward, Color.red, 3.0f);
            if (Physics.Raycast(myTransform.TransformPoint(0, 0, 1), myTransform.forward, out hit, range))
            {
                if (hit.transform.tag == "Enemy")
                {
                    //Debug.Log(hit.transform.name);
                }
            }
            
            nextFire = Time.time + fireRate;
        }
    }
}
