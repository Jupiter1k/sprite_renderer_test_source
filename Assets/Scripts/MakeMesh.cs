﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MakeMesh : MonoBehaviour 
{
    public GameObject[] objects;

    public Color albedoColor;
    public Color specularColor;
    public float glossiness;
    public float emissiveLevel;
    public float emissiveColorBias;

    private List<Vector3> resultVerts;
    private List<Vector3> resultNormals;
    private List<Vector2> resultUVs;
    private List<Color> resultColors;
    private List<int> resultTriangles;
    Mesh mesh;
    Color newColor;

    private string debugInfo = string.Empty;

    void OnGUI()
    {
        GUI.Label(new Rect(10, Screen.height - 40, 1000, 20), debugInfo);
    }

    void Start()
    {
        mesh = new Mesh();
        GetComponent<MeshFilter>().mesh = mesh;
        CombineMeshData();

        foreach (GameObject obj in objects)
        {
            obj.SetActive(false);
        }
    }

    void CombineMeshData()
    {
        float startTime = Time.realtimeSinceStartup;

        mesh.Clear();

        resultVerts = new List<Vector3>();
        resultNormals = new List<Vector3>();
        resultColors = new List<Color>();
        resultUVs = new List<Vector2>();
        resultTriangles = new List<int>();

        //test values
        //albedoColor = new Color(Mathf.Clamp(albedoColor.r, 0.001f, 0.999f), Mathf.Clamp(albedoColor.g, 0.001f, 0.999f), Mathf.Clamp(albedoColor.b, 0.001f, 0.999f));
        //specularColor = new Color(Mathf.Clamp(specularColor.r, 0.001f, 0.999f), Mathf.Clamp(specularColor.g, 0.001f, 0.999f), Mathf.Clamp(specularColor.b, 0.001f, 0.999f));
        //Color albedoColor = new Color(.999f, 0.001f, 0.001f);
        //Color specColor = new Color(0.222f, 0.222f, 0.222f);
        //float smoothness = 0.65f;
        //float emissionLevel = 0.2f;
        //float emissionUseColor = .2f;

        float colorR = Mathf.Clamp(albedoColor.r, 0.001f, 0.999f) + (Mathf.Clamp(specularColor.r, 0.001f, 0.999f) / 1000f);
        float colorG = Mathf.Clamp(albedoColor.g, 0.001f, 0.999f) + (Mathf.Clamp(specularColor.g, 0.001f, 0.999f) / 1000f);
        float colorB = Mathf.Clamp(albedoColor.b, 0.001f, 0.999f) + (Mathf.Clamp(specularColor.b, 0.001f, 0.999f) / 1000f);
        float colorA = glossiness + (Mathf.Clamp(emissiveLevel, .01f, .99f) / 100f) + (Mathf.Clamp(emissiveColorBias, .01f, .99f) / 10000f);

        //Debug.Log(glossiness.ToString("F8") + " " + (Mathf.Clamp(emissiveLevel, .01f, .99f) / 100f).ToString("F8") + " " + (Mathf.Clamp(emissiveColorBias, .01f, .99f) / 10000f).ToString("F8"));

        //colorA = .123456789f;

        //Debug.Log(colorR.ToString("F3") + " " + colorG.ToString("F3") + " " + colorB.ToString("F3") + " " + colorA.ToString("F3"));

        int vertCounter = 0;
        
        foreach (GameObject obj in objects)
        {
            Mesh objMesh = obj.GetComponent<MeshFilter>().mesh;
            newColor = new Color(colorR, colorG, colorB, colorA);

            foreach (int triVert in objMesh.triangles)
            {
                resultTriangles.Add(triVert + vertCounter);
            }

            for (int x = 0; x < objMesh.vertices.Length; x++)
            {
                resultVerts.Add(obj.transform.TransformPoint(objMesh.vertices[x]));
                resultNormals.Add(obj.transform.TransformDirection(objMesh.normals[x]));
                resultColors.Add(newColor);
                resultUVs.Add(objMesh.uv[x]);
                vertCounter++;
            }
        }

        mesh.vertices = resultVerts.ToArray();
        mesh.normals = resultNormals.ToArray();
        mesh.colors = resultColors.ToArray();
        mesh.uv = resultUVs.ToArray();
        mesh.triangles = resultTriangles.ToArray();
        mesh.tangents = CalculateMeshTangents(mesh);

        //foreach (Color vertColor in mesh.colors)
        //{
        //    Debug.Log(vertColor);
        //}
        //Debug.Log(mesh.colors[0].ToString("F7"));

        mesh.RecalculateBounds();
        debugInfo = "It took " + (Time.realtimeSinceStartup - startTime) + " time with " + mesh.triangles.Length / 3 + " triangles";

        //Debug.Log("It took " + (Time.realtimeSinceStartup - startTime) + " time");
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            CombineMeshData();
        }
    }

    public Vector4[] CalculateMeshTangents(Mesh mesh)
    {
        //speed up math by copying the mesh arrays
        int[] triangles = mesh.triangles;
        Vector3[] vertices = mesh.vertices;
        Vector2[] uv = mesh.uv;
        Vector3[] normals = mesh.normals;

        //variable definitions
        int triangleCount = triangles.Length;
        int vertexCount = vertices.Length;

        Vector3[] tan1 = new Vector3[vertexCount];
        Vector3[] tan2 = new Vector3[vertexCount];

        Vector4[] tangents = new Vector4[vertexCount];

        for (long a = 0; a < triangleCount; a += 3)
        {
            long i1 = triangles[a + 0];
            long i2 = triangles[a + 1];
            long i3 = triangles[a + 2];

            Vector3 v1 = vertices[i1];
            Vector3 v2 = vertices[i2];
            Vector3 v3 = vertices[i3];

            Vector2 w1 = uv[i1];
            Vector2 w2 = uv[i2];
            Vector2 w3 = uv[i3];

            float x1 = v2.x - v1.x;
            float x2 = v3.x - v1.x;
            float y1 = v2.y - v1.y;
            float y2 = v3.y - v1.y;
            float z1 = v2.z - v1.z;
            float z2 = v3.z - v1.z;

            float s1 = w2.x - w1.x;
            float s2 = w3.x - w1.x;
            float t1 = w2.y - w1.y;
            float t2 = w3.y - w1.y;

            float r = 1.0f / (s1 * t2 - s2 * t1);

            Vector3 sdir = new Vector3((t2 * x1 - t1 * x2) * r, (t2 * y1 - t1 * y2) * r, (t2 * z1 - t1 * z2) * r);
            Vector3 tdir = new Vector3((s1 * x2 - s2 * x1) * r, (s1 * y2 - s2 * y1) * r, (s1 * z2 - s2 * z1) * r);

            tan1[i1] += sdir;
            tan1[i2] += sdir;
            tan1[i3] += sdir;

            tan2[i1] += tdir;
            tan2[i2] += tdir;
            tan2[i3] += tdir;
        }


        for (long a = 0; a < vertexCount; ++a)
        {
            Vector3 n = normals[a];
            Vector3 t = tan1[a];

            Vector3.OrthoNormalize(ref n, ref t);
            tangents[a].x = t.x;
            tangents[a].y = t.y;
            tangents[a].z = t.z;

            tangents[a].w = (Vector3.Dot(Vector3.Cross(n, t), tan2[a]) < 0.0f) ? -1.0f : 1.0f;
        }

        return tangents;
    }
}
