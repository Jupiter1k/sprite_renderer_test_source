﻿using UnityEngine;
using System.Collections;

public class Detection : MonoBehaviour 
{
    private RaycastHit hit;
    private float checkRate = 0.5f;
    private float nextCheck = 0.0f;
    private Transform myTransform;
    private int range = 5;
    private LayerMask detectionLayer;

	// Use this for initialization
	void Start () 
    {
        SetInitialReferences();
	}

    void SetInitialReferences()
    {
        myTransform = transform;
        detectionLayer = 1 << 9;
    }
	
	// Update is called once per frame
	void Update () 
    {
        DetectItems();
	}

    void DetectItems()
    {
        if (Time.time > nextCheck)
        {
            nextCheck = Time.time + checkRate;

            if (Physics.Raycast(myTransform.position, myTransform.forward, out hit, range, detectionLayer))
            {
                //Debug.Log(hit.transform.name + " is an Item");
            }
        }
    }
}
