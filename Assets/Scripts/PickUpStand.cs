﻿using UnityEngine;
using System.Collections;

public class PickUpStand : MonoBehaviour 
{
    public Transform itemAttachPoint;
    private Weapon pickUpItem;
    private GameObject pickUpItemWorldModel;
    public AudioClip pickUpSound;
    private bool hasItem = true;
    public Weapon[] spawnItems;
    public float itemRespawnSpeed = 10.0f;

	void FixedUpdate () 
    {
        itemAttachPoint.transform.Rotate(0, 1, 0);
	}

    void ItemRespawn()
    {
        LoadPickUpItem(spawnItems[0]);
    }

    public void LoadPickUpItem(Weapon weapon)
    {
        pickUpItem = Weapon.CreateWeapon(weapon);
        pickUpItemWorldModel = (GameObject)Instantiate(pickUpItem.WorldModelPrefab, itemAttachPoint.position, itemAttachPoint.rotation);
        pickUpItemWorldModel.transform.parent = itemAttachPoint;
        hasItem = true;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Player" && hasItem)
        {
            GameManager.MainGame.Player.GetComponent<Player>().PickUpWeapon(pickUpItem);
            GetComponent<AudioSource>().PlayOneShot(pickUpSound);
            hasItem = false;
            Invoke("ItemRespawn", itemRespawnSpeed);
        }
    }

    public Weapon[] SpawnItems
    {
        get { return spawnItems; }
    }
}
