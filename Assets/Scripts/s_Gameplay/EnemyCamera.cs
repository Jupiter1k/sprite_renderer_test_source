﻿using UnityEngine;
using System.Collections;

public class EnemyCamera : MonoBehaviour 
{
    public GameObject enemyGameObject;
    public GameObject visualGameObject;
    public GameObject spriteSheet;
    private Camera thisCamera;
    private Transform firstPersonTransform;
    public GameObject oldEnemyObject;
    public GameObject newEnemyObject;
    private GameObject newEnemyObjectMesh;

    private Material enemySpriteSheetMaterial;
    private RenderTexture enemyRenderTexture;

    private GameManager mainGame;
    private Player player;
    private float cameraYOffset;

    //test
    private int renderCounterTime = 10;
    private int renderCounter = 0;

    void Start()
    {
        mainGame = GameManager.MainGame;
        player = mainGame.Player.GetComponent<Player>();
        cameraYOffset = player.PlayerViewCamPosition.position.y - player.transform.position.y;

        if (mainGame.UseTestEnemyMesh)
        {
            visualGameObject = newEnemyObject;
            newEnemyObjectMesh = (GameObject)Instantiate(mainGame.EnemyMeshTest, newEnemyObject.transform.position + new Vector3(0, -1.0f, 0), newEnemyObject.transform.rotation);
            newEnemyObjectMesh.transform.parent = newEnemyObject.transform;
        }
        else
        {
            visualGameObject = oldEnemyObject;
        }
        
        thisCamera = GetComponent<Camera>();
        thisCamera.fieldOfView = 40.0f;

        enemySpriteSheetMaterial = spriteSheet.GetComponent<Renderer>().material;
        firstPersonTransform = GameManager.MainGame.Player.transform;

        enemyRenderTexture = new RenderTexture(mainGame.SpriteSize, mainGame.SpriteSize, 1);
        enemyRenderTexture.filterMode = mainGame.SpriteFilterMode;
        enemyRenderTexture.antiAliasing = (int)mainGame.SpriteAntiAliasLevel;
        thisCamera.targetTexture = enemyRenderTexture;

        renderCounter = mainGame.SpriteRenderSpeed;

        enemySpriteSheetMaterial.SetTexture("_MainTex", enemyRenderTexture);

        visualGameObject.SetActive(true);
        thisCamera.Render();
        visualGameObject.SetActive(false);
    }

    void Update()
    {
        renderCounter--;

        if (mainGame.OkToRenderEnemy && renderCounter <= 0)
        {
            mainGame.OkToRenderEnemy = false;
            visualGameObject.SetActive(true);
            thisCamera.Render();
            visualGameObject.SetActive(false);
            mainGame.OkToRenderEnemy = true;
            renderCounter = mainGame.SpriteRenderSpeed;
        }

        transform.position = enemyGameObject.transform.position - ((enemyGameObject.transform.position - firstPersonTransform.position).normalized * 3) + new Vector3(0, cameraYOffset, 0);
        transform.LookAt(enemyGameObject.transform.position, Vector3.up);

        //transform.position = firstPersonTransform.position + new Vector3(0, cameraYOffset, 0);

        //thisCamera.fieldOfView = 115/Vector3.Distance(thisCamera.transform.position, enemyGameObject.transform.position);  //works with quad size of 2

        //thisCamera.fieldOfView = 130 / Vector3.Distance(thisCamera.transform.position, enemyGameObject.transform.position);
    }
}
