﻿using UnityEngine;
using System.Collections;

public class Particle : MonoBehaviour 
{
	void Start () 
    {
        GetComponent<AudioSource>().Play();
        GetComponent<ParticleSystem>().Play();
        Destroy(gameObject, GetComponent<ParticleSystem>().duration);
	}
}
