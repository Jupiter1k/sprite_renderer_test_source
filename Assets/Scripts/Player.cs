﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour 
{
    private Weapon currentWeapon;
    private Weapon currentWeaponWorldModel;
    private GameObject currentWeaponViewModel;
    public Transform hands;
    public Canvas playerHUD;
    private AudioSource playerAudioSource;
    public Transform playerViewCam;

    void Start()
    {
        playerAudioSource = GetComponent<AudioSource>();
    }

    public void PickUpWeapon(Weapon weapon)
    {
        if (currentWeapon != null)
        {
            Destroy(currentWeapon);
            //Destroy(currentWeaponWorldModel);
            Destroy(currentWeaponViewModel);
        }

        currentWeapon = Weapon.CreateWeapon(weapon);
        currentWeaponViewModel = (GameObject)Instantiate(currentWeapon.ViewModelPrefab, hands.position, hands.rotation);
        currentWeaponViewModel.transform.parent = hands;
        currentWeapon.InHands = true;
        playerHUD.GetComponent<PlayerHUD>().UpdateWeaponName(currentWeapon.WeaponName);

        Destroy(weapon);
    }

    void Update()
    {
        if (currentWeaponViewModel != null && Input.GetButtonDown("Fire1"))
        {
            currentWeaponViewModel.GetComponent<GunView>().FireWeapon(currentWeapon.ProjectilePrefab, currentWeapon.LaunchSound, playerAudioSource);
        }
    }

    //void OnTriggerEnter(Collider other)
    //{
    //    if (other.transform.tag == "Weapon")
    //    {
    //        PickUpWeapon(other.gameObject);
    //    }
    //}

    public Weapon CurrentWeapon
    {
        get { return currentWeapon; }
    }

    public Transform PlayerViewCamPosition
    {
        get { return playerViewCam; }
    }
}
