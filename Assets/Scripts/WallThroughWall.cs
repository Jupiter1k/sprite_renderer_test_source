﻿using UnityEngine;
using System.Collections;

public class WallThroughWall : MonoBehaviour 
{
    void OnEnable()
    {
        gameObject.layer = LayerMask.NameToLayer("NotSolid");
    }

    void OnDisable()
    {
        gameObject.layer = LayerMask.NameToLayer("Default");
    }
}
