﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerHUD : MonoBehaviour 
{
    public Text currentWeaponName;
    private Player player;

    void Start()
    {
        player = GameManager.MainGame.Player.GetComponent<Player>();
    }

    public void UpdateWeaponName(string weaponName)
    {
        currentWeaponName.text = weaponName;
    }

    //public void playerHUDNewWeapon(string inWeaponName)
    //{
    //    currentWeaponName.text = inWeaponName;
    //}

    //public float PropulsionSpeed
    //{
    //    get { return propulsionSpeed; }
    //}

}
