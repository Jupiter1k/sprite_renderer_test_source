﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum GameState { On, Off }

public enum SpriteAntiAliasLevelEnum { Low = 1, Medium = 2, High = 4, UltraHigh = 8 }


public class GameManager : MonoBehaviour 
{
    private static GameManager mainGame;
    private List<GameObject> enemies;
    private GameState currentGameState;
    public Camera startCamera;
    public GameObject playerPrefab;
    private GameObject player;
    public GameObject playerSpawn1;
    public GameObject[] pickUpStands;
    private bool okToRenderEnemy = true;
    public GameObject enemyMeshTestNode;
    public GameObject enemyMeshTestBounds;
    private GameObject enemyMeshTest;

    [Header("Test Settings")]
    public bool useTestEnemyMesh;
    public int numberOfEnemies;

    [Header("Sprite Settings")]
    public int spriteSize;
    public FilterMode spriteFilterMode;
    public SpriteAntiAliasLevelEnum spriteAntiAliasLevel;
    public int spriteRenderSpeed;

    void Awake()
    {
        mainGame = this;
        currentGameState = GameState.Off;
        enemies = new List<GameObject>();
        pickUpStands[0].GetComponent<PickUpStand>().LoadPickUpItem(pickUpStands[0].GetComponent<PickUpStand>().SpawnItems[0]);
        pickUpStands[1].GetComponent<PickUpStand>().LoadPickUpItem(pickUpStands[1].GetComponent<PickUpStand>().SpawnItems[0]);
        pickUpStands[2].GetComponent<PickUpStand>().LoadPickUpItem(pickUpStands[2].GetComponent<PickUpStand>().SpawnItems[0]);
        enemyMeshTest = enemyMeshTestNode.transform.GetChild(0).gameObject;
        enemyMeshTestNode.SetActive(false);
        enemyMeshTestBounds.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () 
    {
        if (currentGameState == GameState.Off && Input.GetKeyDown(KeyCode.Space))
        {
            startCamera.gameObject.SetActive(false);
            SpawnPlayer();
            currentGameState = GameState.On;
        }
	}

    public void SpawnPlayer()
    {
        player = (GameObject)Instantiate(playerPrefab, playerSpawn1.transform.position, playerSpawn1.transform.rotation);
    }

    public void KillEnemy(GameObject enemyToKill, GameObject enemyShadowBlob)
    {
        enemies.Remove(enemyToKill);
        Destroy(enemyToKill);
        Destroy(enemyShadowBlob);
    }

    public static GameManager MainGame
    {
        get { return mainGame; }
    }

    public GameState CurrentGameState
    {
        get { return currentGameState; }
        set { currentGameState = value; }
    }

    public List<GameObject> Enemies
    {
        get { return enemies; }
    }

    public Camera StartCamera
    {
        get { return startCamera; }
    }

    public GameObject Player
    {
        get { return player; }
    }

    public GameObject EnemyMeshTest
    {
        get { return enemyMeshTest; }
    }

    public bool UseTestEnemyMesh
    {
        get { return useTestEnemyMesh; }
    }

    public bool OkToRenderEnemy
    {
        get { return okToRenderEnemy; }
        set { okToRenderEnemy = value; }
    }

    public int SpriteSize
    {
        get { return spriteSize; }
    }

    public FilterMode SpriteFilterMode
    {
        get { return spriteFilterMode; }
    }

    public SpriteAntiAliasLevelEnum SpriteAntiAliasLevel
    {
        get { return spriteAntiAliasLevel; }
    }

    public int NumberOfEnemies
    {
        get { return  numberOfEnemies; }
    }

    public int SpriteRenderSpeed
    {
        get { return spriteRenderSpeed; }
    }
}
