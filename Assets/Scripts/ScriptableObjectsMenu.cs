﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;


public class NewBehaviourScript
{
    [MenuItem("Assets/Create/ScriptableWeapon")]
    public static void CreateWeaponAsset()
    {
        Weapon newWeaponAsset = ScriptableObject.CreateInstance<Weapon>();

        AssetDatabase.CreateAsset(newWeaponAsset, "Assets/Prefabs/Weapons/newWeapon.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = newWeaponAsset;
    }
}
#endif
