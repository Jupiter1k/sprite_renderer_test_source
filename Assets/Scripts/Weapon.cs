﻿using UnityEngine;
using System.Collections;

public class Weapon : ScriptableObject 
{
    public string weaponName;
    public GameObject worldModelPrefab;
    public GameObject viewModelPrefab;
    public GameObject projectilePrefab;
    private bool inHands = false;
    private GameObject viewModel;
    public AudioClip launchSound;

    public static Weapon CreateWeapon(Weapon inWeaponData)
    {
        Weapon returnWeapon = ScriptableObject.CreateInstance<Weapon>();

        returnWeapon.WeaponName = inWeaponData.WeaponName;
        returnWeapon.WorldModelPrefab = inWeaponData.WorldModelPrefab;
        returnWeapon.ViewModelPrefab = inWeaponData.ViewModelPrefab;
        returnWeapon.ProjectilePrefab = inWeaponData.ProjectilePrefab;
        returnWeapon.LaunchSound = inWeaponData.LaunchSound;

        return returnWeapon;
    }

    public string WeaponName
    {
        get { return weaponName; }
        set { weaponName = value; }
    }

    public GameObject WorldModelPrefab
    {
        get { return worldModelPrefab; }
        set { worldModelPrefab = value; }
    }

    public GameObject ViewModelPrefab
    {
        get { return viewModelPrefab; }
        set { viewModelPrefab = value; }
    }

    public GameObject ProjectilePrefab
    {
        get { return projectilePrefab; }
        set { projectilePrefab = value; }
    }

    public AudioClip LaunchSound
    {
        get { return launchSound; }
        set { launchSound = value; }
    }

    public bool InHands
    {
        get { return inHands; }
        set { inHands = value; }
    }
}
