﻿using UnityEngine;
using System.Collections;

public enum PropulsionMode { Propelled, Unpropelled }

public class Projectile : MonoBehaviour 
{
    public enum ContactExplodeMode { Everything, OnlyWalls, OnlyFloors, OnlyWallsAndFloors, OnlyEnemies, OnlyTeammates, OnlyPlayers }

    
    [Header("Values")]
    public float blastRadius = 10.0f;
    public float explosionPower = 20.0f;
    public float grenadeTimer = 3.0f;
    public PropulsionMode propulsionType;
    public float launchPower;
    public float propulsionSpeed;
    public ContactExplodeMode contactExplodeType;

    [Header("Sounds")]
    public AudioClip contactBounce;
    public AudioClip launch;

    [Header("Effects")]
    public ParticleSystem explosionEffect;
    public ParticleSystem trailEffect;

    [Header("Other")]
    public LayerMask explosionLayers;


    private AudioSource audioSource;
    private Collider[] hitColliders;
    private float timeSpawned;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        timeSpawned = Time.time;
    }


    int ExplosionDamage(Vector3 pointOfExplosion, Vector3 enemyPosition, float blastRadius)
    {
        float distance = Vector3.Distance(pointOfExplosion, enemyPosition);

        return (int)Mathf.Clamp((blastRadius - distance), 0.0f, 1000.0f) * 5;
    }

    void ExplosionForce(Vector3 pointOfExplosion)
    {
        hitColliders = Physics.OverlapSphere(pointOfExplosion, blastRadius, explosionLayers);

        foreach (Collider hitCol in hitColliders)
        {
            if (hitCol.GetComponent<Rigidbody>() != null)
            {
                if (hitCol.gameObject.tag == "Enemy")
                {
                    if (hitCol.gameObject.GetComponent<EnemyAI>().ApplyDamage(ExplosionDamage(pointOfExplosion, hitCol.transform.position, blastRadius))) // see if enemy died
                    {
                        hitCol.GetComponent<NavMeshAgent>().enabled = false;
                        hitCol.GetComponent<Rigidbody>().isKinematic = false;
                        hitCol.GetComponent<Rigidbody>().AddExplosionForce(explosionPower, pointOfExplosion, blastRadius, 1.0f, ForceMode.Impulse);

                        hitCol.gameObject.GetComponent<EnemyAI>().KillEnemy();
                    }
                    else // enemy survived, knockback
                    {
                        hitCol.GetComponent<NavMeshAgent>().enabled = false;
                        hitCol.GetComponent<Rigidbody>().isKinematic = false;
                        hitCol.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
                        hitCol.GetComponent<Rigidbody>().AddExplosionForce(explosionPower, pointOfExplosion, blastRadius, .5f, ForceMode.Impulse);
                        hitCol.GetComponent<EnemyAI>().InAir = true;
                    }
                }
                else
                {
                    hitCol.GetComponent<Rigidbody>().isKinematic = false;
                    hitCol.GetComponent<Rigidbody>().AddExplosionForce(explosionPower, pointOfExplosion, blastRadius, 1.0f, ForceMode.Impulse);
                }
            }
        }
    }

    void OnCollisionEnter(Collision col)
    {
        switch (contactExplodeType)
        {
            case ContactExplodeMode.Everything:
                ExplosionForce(col.contacts[0].point);
                Destroy(gameObject);
                Instantiate(explosionEffect, col.contacts[0].point, Quaternion.identity);
                break;
            case ContactExplodeMode.OnlyEnemies:
                if (col.transform.tag == "Enemy")
                {
                    ExplosionForce(col.contacts[0].point);
                    Destroy(gameObject);
                    Instantiate(explosionEffect, col.contacts[0].point, Quaternion.identity);
                }
                break;
            default:
                audioSource.PlayOneShot(contactBounce, 1f);
                break;
        }
    }

    void FixedUpdate()
    {
        if (Time.time - timeSpawned > grenadeTimer)
        {
            ExplosionForce(transform.position);
            Destroy(gameObject);
            Instantiate(explosionEffect, transform.position, Quaternion.identity);
        }
    }

    public PropulsionMode PropulsionType
    {
        get { return propulsionType; }
    }

    public AudioClip LaunchSound
    {
        get { return launch; }
    }

    public float LaunchPower
    {
        get { return launchPower; }
    }

    public float PropulsionSpeed
    {
        get { return propulsionSpeed; }
    }
}
