﻿using UnityEngine;
using System.Collections;

public class LookAtPlayerCamera : MonoBehaviour 
{
    private Camera playerCam;

    void Awake()
    {
        playerCam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
    }
    
    void FixedUpdate () 
    {
        transform.LookAt(playerCam.transform);
	}
}
